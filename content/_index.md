## Centerlization is an internet antipattern.

The internet is destributed by nature. However, more and more services
are centerlizing the internet. These include common sites like [Slack]<https://slack.com>, 
[Twitter]<https://twitter>, [Youtube]<https://youtube.com>, and others
are centerlizing the internet with vendor lockin, the inabbility to self
host, own your own data, and create compatible, simular services.

While we cannot fully remove the centerlization of services, we can try
to promote the use of decenterlizes and destributed alternatives to many
popular sites and services.

<!---
A list of centerlized, proprietary services and decenterlized or distributed
alternatives.
-->
